Test technique

Avant de lancer le projet:
- npm i

Commandes dispo :
- npm run start
- npm run test
- npm run storybook

Mise en place de quelques tests (snapshots et fonctionnels) et d'un storybook.
Les destinations sont affichées par ordre alphabétique avec un petit rappel du nom de la nouvelle destination lorsque celle-ci change.

