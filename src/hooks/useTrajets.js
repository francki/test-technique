import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import {
  getTrips,
  getTripsWithDepartureStop,
} from "../reducer/trajets/trajetsActions";

const useTrajets = (departureStop) => {
  const [trips, setTrips] = useState([]);
  const [loadingTrips, setloadingTrips] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setloadingTrips(true)
        let data;
        if (departureStop !== "" || departureStop !== null) {
          data = await getTripsWithDepartureStop(departureStop);
        } else {
          data = await getTrips();
        }
        if (data && Array.isArray(data.data)) {
          setloadingTrips(false)
          setTrips(data.data);
        } else {
          setloadingTrips(false)
          toast.error("La réponse du serveur est invalide");
        }
      } catch (e) {
        setloadingTrips(false)
        setTrips([]);
        toast.error("Une erreur est survenue");
      }
    };
    fetchData();
  }, [departureStop]);

  return { trips, loadingTrips };
};

export { useTrajets };
