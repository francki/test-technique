import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { getStops } from "../reducer/arrets/arretsActions";

const useStops = () => {
  const [stops, setStops] = useState([]);
  const [loadingStops, setLoadingStops] = useState(false);

  const getStopsList = async () => {
    setLoadingStops(true)
    const data = await getStops();
    if (data) {
      setLoadingStops(false)
      setStops(data.data);
    } else {
      setLoadingStops(false)
      toast.error("No data from server for Stops");
    }
  };
  useEffect(() => {
    try {
      getStopsList();
    } catch (e) {
      setLoadingStops(false)
      setStops([]);
      toast.error("Une erreur est survenue");
    }
  }, []);
  return { stops, loadingStops };
};

export { useStops };
