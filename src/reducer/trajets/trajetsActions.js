import axios from "axios";


const getTrips = async () => {
    const res = await axios.get("https://6130d11c8066ca0017fdaa97.mockapi.io/trips");
    return res
}

const getTripsWithDepartureStop = async (departureStop) => {
    const res = await axios.get(`https://6130d11c8066ca0017fdaa97.mockapi.io/trips?departureStop=${encodeURIComponent(departureStop)}`);
    return res
}

export {getTrips, getTripsWithDepartureStop}