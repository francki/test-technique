import axios from "axios";

const bookTrip = async (tripId) => {
    const res = await axios.put(`https://6130d11c8066ca0017fdaa97.mockapi.io/book/${tripId}`);
    if (res.status === 200) {
        return res.data
    }
    return {}
}

export {bookTrip}