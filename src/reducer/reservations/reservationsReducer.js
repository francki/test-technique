const reservationsReducer = (state, action) => {
    switch (action.type) {
        case 'SET_LOADING':
            return {
                ...state,
                loading: true
            }
        case 'CLEAR_LOADING':
            return {
                ...state,
                loading: false
            }
        default:
            return state
    }
}
export default reservationsReducer;