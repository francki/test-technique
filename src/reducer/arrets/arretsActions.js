import axios from "axios";

const getStops = async () => {
    const res = await axios.get('https://6130d11c8066ca0017fdaa97.mockapi.io/stops');
    return res
}

export {getStops}