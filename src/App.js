import SinglePage from "./components/05-pages/SinglePage";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <ToastContainer theme={"dark"} />
      <SinglePage/>
    </div>
  );
}

export default App;
