import React from 'react';
import { Form } from 'react-bootstrap';

import "./Select.scss"

function Select({ options, value, onChange }) {
    return (
        <div className='select-container'>
            <h4 className="select-title">Sélectionnez un arrêt de départ</h4>
            <Form.Select  value={value} onChange={onChange}>
                <option default value="">Sélectionnez un arrêt de départ</option>
                {options?.map((option, index) => (
                    <option key={index} value={option}>{option}</option>
                ))}
            </Form.Select>
        </div>
    );
}

export default Select;
