import React from "react";
import TravelCard from "./TravelCard";

export default {
  title: "Components/02-molecules/TravelCard",
  component: TravelCard,
};

export const Default = () => (
  <TravelCard
    id={1}
    dep="Paris"
    arr="Lyon"
    heure_dep="10h00"
    heure_arr="12h00"
    onClick={() => console.log("Reservation")}
  />
);
