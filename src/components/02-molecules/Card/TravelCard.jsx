import React from 'react';
import Button from '../../01-atoms/Button/Button'
import { Row, Col, Container } from 'react-bootstrap';
import { Card as CardBootstrap } from 'react-bootstrap';

import "./TravelCard.scss";

function TravelCard(props) {
  const { id, dep, arr, heure_dep, heure_arr, onClick } = props;

  return (
    <CardBootstrap className="item-travel-container">
      <CardBootstrap.Body>
        <Container>
          <Row>
            <Col className='item-travel-label'>DEP</Col>
            <Col className='item-travel-id'>#{id}</Col>
            <Col className='item-travel-label'>ARR</Col>
          </Row>
          <Row>
            <Col className='item-travel-dep'>{dep}</Col>
            <Col> </Col>
            <Col className='item-travel-arr'>{arr}</Col>
          </Row>
          <Row>
            <Col className='item-travel-hours'>{heure_dep}</Col>
            <Col></Col>
            <Col className='item-travel-hours'>{heure_arr}</Col>
          </Row>
        </Container>
      </CardBootstrap.Body>
      <CardBootstrap.Body>
        <Button className="single-page-btn" onClick={onClick}>Réserver</Button>
      </CardBootstrap.Body>
    </CardBootstrap>
  );
}

export default TravelCard;
