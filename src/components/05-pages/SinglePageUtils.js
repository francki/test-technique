const sortTripsByArrivalStop = (trips) => {
    return trips.sort((a, b) => a.arrivalStop.localeCompare(b.arrivalStop));
}

export {sortTripsByArrivalStop}