import React, { useState, useReducer, useEffect } from 'react';
import { toast } from "react-toastify";
import Spinner from '../01-atoms/Spinner/Spinner'
import Select from '../02-molecules/Select/Select';
import { useStops } from "../../hooks/useStops";
import { useTrajets } from '../../hooks/useTrajets';
import { bookTrip } from '../../reducer/reservations/reservationsActions';
import TravelCardList from '../03-organisms/TravelCardList/TravelCardList';

import reservationsReducer from '../../reducer/reservations/reservationsReducer';

import "./SinglePage.scss";

function SinglePage() {
    const [departureStop, setDepartureStop] = useState('');
    const { stops, loadingStops } = useStops();
    const { trips, loadingTrips } = useTrajets(departureStop);

    const [reservationsState, dispatchReservations] = useReducer(reservationsReducer, { loading: false });

    function handleStopChange(event) {
        const selectedStop = event.target.value;
        setDepartureStop(selectedStop);
    }

    async function handleReservation(tripId) {
        try {
            dispatchReservations({ type: 'SET_LOADING' });
            const res = await bookTrip(tripId);
            if (res.message) {
                console.log(res.message)
            } else {
                dispatchReservations({ type: 'CLEAR_LOADING' });
                toast.success('Réservation effectuée avec succès');
            }
        } catch (error) {
            dispatchReservations({ type: 'CLEAR_LOADING' });
            toast.error('Une erreur est survenue lors de la réservation');
        }
    }

    return (
        <div className='single-page-container'>
            <h1 className='single-page-title'>Cheap Mobility</h1>
            {!reservationsState.loading ?
                <>
                    {!loadingStops ?
                        (
                            <>
                                {stops?.length > 0 ? (
                                    <Select options={stops} value={departureStop} onChange={handleStopChange} />
                                ) :
                                    (<p>Aucun arrêt de départ disponible</p>)
                                }
                            </>
                        )
                        :
                        (<Spinner />)
                    }
                    {!loadingTrips ?
                        (<>
                            {trips?.length > 0 ? (
                                <TravelCardList trips={trips} onClick={(id) => handleReservation(id)} />
                            ) :
                                (<p>Aucun trajet disponible pour cet arrêt</p>)
                            }
                        </>)
                        :
                        (<Spinner />)
                    }
                </>
                :
                (<Spinner />)
            }
        </div>
    );
}

export default SinglePage;