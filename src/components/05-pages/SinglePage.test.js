import {sortTripsByArrivalStop} from './SinglePageUtils';

describe('sortTripsByArrivalStop', () => {
    test('return empty array when input is empty', () => {
      expect(sortTripsByArrivalStop([])).toEqual([]);
    });
  
    test('sort trips by arrivalStop in ascending order', () => {
      const trips = [
        { id: 1, arrivalStop: 'B' },
        { id: 2, arrivalStop: 'A' },
        { id: 3, arrivalStop: 'C' },
      ];
  
      const sortedTrips = sortTripsByArrivalStop(trips);
  
      expect(sortedTrips).toEqual([
        { id: 2, arrivalStop: 'A' },
        { id: 1, arrivalStop: 'B' },
        { id: 3, arrivalStop: 'C' },
      ]);
    });
  
    test('return same array when sorted by arrivalStop', () => {
      const trips = [
        { id: 1, arrivalStop: 'A' },
        { id: 2, arrivalStop: 'B' },
        { id: 3, arrivalStop: 'C' },
      ];
  
      const sortedTrips = sortTripsByArrivalStop(trips);
  
      expect(sortedTrips).toEqual([
        { id: 1, arrivalStop: 'A' },
        { id: 2, arrivalStop: 'B' },
        { id: 3, arrivalStop: 'C' },
      ]);
    });
  });
  