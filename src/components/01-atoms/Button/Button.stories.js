import React from "react";
import Button from "./Button";

export default {
  title: "Components/02-molecules/Button",
  component: Button,
};
export const Secondary = () => <Button variant="secondary" content="Secondary" />;
export const Primary = () => <Button variant="primary" content="Primary" />;
