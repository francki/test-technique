import React from "react";
import Button from "./Button";
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const tree = renderer
    .create(
      <Button
        variant="primary"
        content="Submit"
        onClick={() => console.log("click")}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
