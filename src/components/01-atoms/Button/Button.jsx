import { Button as ButtonBootstrap } from 'react-bootstrap';

function Button({variant, content="Réserver", onClick}) {
  return (
    <>
      <ButtonBootstrap onClick={onClick} variant={variant}>{content}</ButtonBootstrap>
    </>
  );
}

export default Button;