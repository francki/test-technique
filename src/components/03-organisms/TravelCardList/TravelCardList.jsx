import React from 'react';
import TravelCard from '../../02-molecules/Card/TravelCard'
import { sortTripsByArrivalStop } from '../../05-pages/SinglePageUtils';

import "./TravelCardList.scss"

function TravelCardList({ trips, onClick }) {
    return (
            sortTripsByArrivalStop(trips).map((trip, index) => (
                <div className="travel-card-list-container" key={index}>
                    <div className='travel-card-list-indicator'>{index >= 0 && trips[index - 1]?.arrivalStop !== trip?.arrivalStop && <p>{trips[index]?.arrivalStop}</p>}</div>
                    <TravelCard
                        className="travel-card-list-card"
                        id={trip?.id}
                        dep={trip?.departureStop}
                        arr={trip?.arrivalStop}
                        heure_dep={new Date(trip?.departureTime).toLocaleTimeString().slice(0, -3)}
                        heure_arr={new Date(trip?.arrivalTime).toLocaleTimeString().slice(0, -3)}
                        onClick={() => onClick(trip?.id)}>
                    </TravelCard>
                </div>
            ))
    );
}

export default TravelCardList;